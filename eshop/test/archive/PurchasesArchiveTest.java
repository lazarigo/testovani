package archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class PurchasesArchiveTest
{
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Mock
    private HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive;

    ShoppingCart cart;

    @Test
    void putOrderToPurchasesArchiveTest()
    {
        ArrayList<Order> orderArchive = new ArrayList<>();
        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, "a", "ad");
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        archive.putOrderToPurchasesArchive(order);
        assertTrue(orderArchive.contains(order));

    }



    @Test
    void printItemPurchaseStatisticsTest()
    {
        String res = "ITEM PURCHASE STATISTICS:\r\nITEM  Item   ID 1   NAME name   CATEGORY category   PRICE 12.4   LOYALTY POINTS 12   HAS BEEN SOLD 1 TIMES\r\n";
        Item item = new StandardItem(1, "name", 12.4f, "category", 12);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        ShoppingCart cart = new ShoppingCart(items);
        Order order = new Order(cart, "name", "adress");

        PurchasesArchive archive = new PurchasesArchive();

        archive.putOrderToPurchasesArchive(order);
        archive.printItemPurchaseStatistics();
        assertEquals(res, outContent.toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSoldTest()
    {
        ArrayList<Order> orderArchive = new ArrayList<>();
        ShoppingCart cart = new ShoppingCart();
        Item item = new StandardItem(1, "name", 12, "cat", 12);
        cart.addItem(item);
        Order order = new Order(cart, "a", "ad");
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        archive.putOrderToPurchasesArchive(order);
        assertEquals(archive.getHowManyTimesHasBeenItemSold(item), 0);
    }
}
