package storage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import shop.StandardItem;
import shop.Item;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {

    @Test
    void testConstructor() {
        Item refItem = new StandardItem(0,"name",20,"cat",1);
        int count = 0;

        ItemStock itemStock = new ItemStock(refItem);

        assertEquals(0, itemStock.getCount());
        assertEquals(refItem, itemStock.getItem());
    }

    @ParameterizedTest
    @MethodSource("provideStandardItems")
    void testChangeCount(boolean inc,int times,int expected) {
        Item refItem = new StandardItem(0,"name",20,"cat",1);
        ItemStock itemStock = new ItemStock(refItem);

        if (inc) {
            for (int i = 0; i < times; i++) {
                itemStock.IncreaseItemCount(1);
            }
        }
        else {
            for (int i = 0; i < times; i++) {
                itemStock.decreaseItemCount(1);
            }
        }
        assertEquals(expected, itemStock.getCount());

    }

    private static Stream<Arguments> provideStandardItems(){
        return Stream.of(
                Arguments.of(true, 0, 0),
                Arguments.of(false, 0, 0),
                Arguments.of(true, 5, 5),
                Arguments.of(false, 6, -6)
        );
    }
}
