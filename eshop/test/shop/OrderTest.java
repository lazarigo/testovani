package shop;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {

    @Test
    void testConstructor1() {
        ArrayList<Item> items = new ArrayList<>();
        StandardItem testItem = new StandardItem(1,"test",20,"testcat",0);
        items.add(testItem);
        String customerName = "testCustomer";
        String customerAddress = "testAddr";
        ShoppingCart shoppingCart = new ShoppingCart(items);

        Order order = new Order(shoppingCart,customerName,customerAddress);

        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(items, order.getItems());
        assertEquals(0, order.getState());

    }

    @Test
    void testConstructor2() {
        ArrayList<Item> items = new ArrayList<>();
        StandardItem testItem = new StandardItem(1,"test",20,"testcat",0);
        items.add(testItem);
        String customerName = "testCustomer";
        String customerAddress = "testAddr";
        int state = 999;
        ShoppingCart shoppingCart = new ShoppingCart(items);

        Order order = new Order(shoppingCart,customerName,customerAddress,state);

        assertEquals(customerAddress, order.getCustomerAddress());
        assertEquals(customerName, order.getCustomerName());
        assertEquals(items, order.getItems());
        assertEquals(state, order.getState());
    }

    @Test
    void testConstructorWithNullShoppingCart() {
        String customerName = "testCustomer";
        String customerAddress = "testAddr";
        int state = 999;
        ShoppingCart shoppingCart = null;

        assertThrows(NullPointerException.class, ()-> new Order(shoppingCart,customerName,customerAddress,state));
    }

}
