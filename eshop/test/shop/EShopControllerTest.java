package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.testng.annotations.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import storage.NoItemInStorage;
import storage.Storage;

import java.io.PrintStream;
import java.io.ByteArrayOutputStream;

import static shop.EShopController.purchaseShoppingCart;
import static org.testng.AssertJUnit.assertEquals;


public class EShopControllerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void test1() throws NoItemInStorage {
        EShopController.startEShop();
        Item item = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        EShopController.newCart();
        Storage storage = EShopController.getStorage();
        storage.insertItems(item, 1);
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(item);
        purchaseShoppingCart(newCart, "Libuse Novakova", "Kosmonautu 25, Praha 8");

        assertEquals(storage.getItemCount(item), 0);
    }

    @Test
    void test2() throws NoItemInStorage {
        EShopController.startEShop();
        Item item = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        Storage storage = EShopController.getStorage();
        storage.insertItems(item, 1);
        storage.removeItems(item,1);
        storage.insertItems(item, 1);
        EShopController.newCart();
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(item);

        purchaseShoppingCart(newCart, "Libuse Novakova", "Kosmonautu 25, Praha 8");
        assertEquals(0, storage.getItemCount(item.getID()));
    }

    @Test
    @Order(1)
    void test3() throws NoItemInStorage {
        EShopController.startEShop();
        Item item = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        EShopController.newCart();
        ShoppingCart newCart = new ShoppingCart();
        Item item2 = new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013");
        Storage storage = EShopController.getStorage();
        storage.insertItems(item, 1);
        storage.insertItems(item2, 3);
        storage.removeItems(item, 1);
        purchaseShoppingCart(newCart, "fs", "fsd");

        String str = outContent.toString();
        Assertions.assertEquals("", outContent.toString());
    }

    @Test
    void testcase4() throws NoItemInStorage {
        EShopController.startEShop();
        Item item = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        EShopController.newCart();
        ShoppingCart newCart = new ShoppingCart();
        Storage storage = EShopController.getStorage();
        storage.insertItems(item, 2);
        storage.removeItems(item, 1);

        Exception exception = Assertions.assertThrows(Exception.class, () -> storage.removeItems(item, 2));
        assertEquals(exception.getMessage(), "No item in storage");

    }

    @Test
    void test5() {
        EShopController.startEShop();
        Item item = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        EShopController.newCart();
        ShoppingCart newCart = new ShoppingCart();
        Storage storage = EShopController.getStorage();
        newCart.addItem(item);

        Exception exception = Assertions.assertThrows(Exception.class, () -> purchaseShoppingCart(newCart, "Libuse Novakova", "Kosmonautu 25, Praha 8"));
        assertEquals(exception.getMessage(), "No item in storage");
    }
}
