package shop;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StandardItemTest {

    @Test
    void testConstructor() {
        int id = 0;
        String name = "name";
        float price = 4000;
        String category = "category";
        int loyaltyPoints = -49;

        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);

        assertEquals(id, item.getID());
        assertEquals(name, item.getName());
        assertEquals(price, item.getPrice());
        assertEquals(category, item.getCategory());
        assertEquals(loyaltyPoints, item.getLoyaltyPoints());
    }

    @Test
    void testCopy() {
        int id = 0;
        String name = "name";
        float price = 4000;
        String category = "category";
        int loyaltyPoints = -49;
        StandardItem item = new StandardItem(id, name, price, category, loyaltyPoints);

        StandardItem copy = item.copy();

        assertEquals(item, copy);
    }

    @ParameterizedTest
    @MethodSource("provideStandardItems")
    void testEquals(StandardItem item1, StandardItem item2, boolean expected) {
        assertEquals(expected, item1.equals(item2));
    }

    private static Stream<Arguments> provideStandardItems() {
        StandardItem item = new StandardItem(5, "matej", 5600, "adsdf", 134);

        return Stream.of(
                Arguments.of(new StandardItem(1,"a",1,"c",0),new StandardItem(1,"a",1,"c",0),true),
                Arguments.of(new StandardItem(3,"a",1,"c",0),new StandardItem(3,"a",1,"c",2),false)
                ,Arguments.of(item,item,true)
        );
    }
}