package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Test;
import static cz.fel.cvut.ts1.Math.factorial;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MathTest {
    @Test
    public void factorialTest() {
        assertEquals(factorial(3),6);
    }
}
