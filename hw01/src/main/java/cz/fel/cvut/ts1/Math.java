package cz.fel.cvut.ts1;

public class Math {
    public static long factorial (int n) {
        if (n == 1) return 1;
        else return n * factorial(n-1);
    }

    public int factorial2(int n) {
        for(int i=n; i>0; i--) {
            n *= i;
        }
        return n;
    }
}
