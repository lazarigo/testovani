package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorPrintTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void printOutput_stdOutRedirected_correctMessageCaptured() {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        c.add(2,3);

        //ASSERT
        assertEquals("5", outContent.toString());
    }

    @Test
    public void printOutput_stdOutRedirected_correctMessageCaptured2() {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        c.add(5,3);

        //ASSERT
        assertEquals("8", outContent.toString());
    }
}
