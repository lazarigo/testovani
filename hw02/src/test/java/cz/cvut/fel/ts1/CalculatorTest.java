package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


public class CalculatorTest {

    @Test
    public void add_2and3_5() {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        int res = c.add(2,3);

        //ASSERT
        assertEquals(5, res);
    }

    @Test
    public void subtract_7and4_3() {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        int res = c.subtract(7,4);

        //ASSERT
        assertEquals(3, res);
    }

    @Test
    public void multiply_7and5_35() {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        int res = c.multiply(7, 5);

        //ASSERT
        assertEquals(35, res);
    }

    @Test
    public void divide_20and5_4() throws Exception {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        int res = c.divide(20, 5);

        //ASSERT
        assertEquals(4, res);
    }

    @Test
    public void divide_9and0_error() {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT + ASSERT 1
        Exception exception = Assertions.assertThrows(Exception.class, () -> c.divide(9,0));

        String expectedMessage = "Pokus o deleni nulou";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest(name = "add({0},{1}) = {2}")
    @CsvFileSource(resources = "/input.csv", numLinesToSkip = 1)
    public void additionTest(int a, int b, int c) {
        //ARRANGE
        Calculator cal = new Calculator();

        //ACT
        int res = cal.add(a,b);

        //ASSERT
        assertEquals(c, res);
    }

    @ParameterizedTest(name = "add({0},{1}) = {2}")
    @CsvSource(value = {"2,3,5","1,2,3"})
    public void additionTest2(int a, int b, int c) {
        //ARRANGE
        Calculator cal = new Calculator();

        //ACT
        int res = cal.add(a, b);

        //ASSERT
        assertEquals(c, res);
    }

    private static Stream<Arguments>providerMethod() {
        return Stream.of(
                Arguments.of(1,2,3),
                Arguments.of(3,2,5)
        );
    }

    @ParameterizedTest(name = "add({0},{1}) = {2}")
    @MethodSource(value = "providerMethod")
    public void additionTest3(int a, int b, int c) {
        //ARRANGE
        Calculator cal = new Calculator();

        //ACT
        int res = cal.add(a, b);

        //ASSERT
        assertEquals(c, res);
    }

    @ParameterizedTest
    @ValueSource(ints = {2,-4,8,10})
    public void isEvenTest_positive(int a) {
        //ARRANGE
        Calculator c = new Calculator();

        //ACT
        boolean res = c.isEven(a);

        //ASSERT
        assertTrue(res);
    }
}
